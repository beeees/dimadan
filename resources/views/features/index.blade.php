@extends('layouts.layout')

@section('content')
    <h1>Преимущества</h1>
    <a href="{{url('features/create')}}">Добавить преимущество</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Размер</th>
            <th scope="col">Преимущество</th>
            <th scope="col">Изменить</th>
            <th scope="col">Удалить</th>
        </tr>
        </thead>
        <tbody>
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        @foreach($features as $feature)
            <tr>
                <td scope="row" style="text-align: left;">
                    {{($id = $feature->main_bag_id) != 0 ? \App\Models\MainBag::find($id)->size : 'Все'}}
                </td>
                <td scope="row">{{$feature->$feature}}</td>
                <td><a href="{{url('features', [$feature->id, 'edit'])}}">Изменить</a></td>
                <td>
                    <form action="{{url('features', [$feature->id])}}" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" class="btn btn-danger" value="Удалить"/>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
