@extends('layouts.layout')

@section('content')
    <h1>Редактирование преимущества</h1>
    <hr>
    <div class="form-group">
        <label for="main_bag_id">Размер:
            {{($bag = \App\Models\MainBag::find($feature->main_bag_id)) ? $bag->size : 'Все' }}
        </label>
    </div>
    <div class="form-group">
        <label for="feature">Преимущество: {{$feature->feature}}
        </label>
    </div>
    <a href="{{route('features.index')}}"><button>К списку</button></a>
@endsection