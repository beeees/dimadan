@extends('layouts.layout')

@section('content')
    <h1>Редактирование преимущества</h1>
    <hr>
    <form action="{{url('features', [$feature->id])}}" method="post">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group">
            <label for="main_bag_id">Размер</label>
            <select id="main_bag_id" name="main_bag_id">
                <option value="0" {{$feature->main_bag_id == 0 ? 'selected' : ''}}>Все</option>
                @foreach(\App\Models\MainBag::all() as $bag)
                    <option value="{{$bag->id}}"
                        {{$feature->main_bag_id == $bag->id ? 'selected' : ''}}
                    >{{$bag->size}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="feature">Преимущество</label>
            <input type="text" width="150px" height="100px" class="form-control" id="feature"
                   name="feature" value="{{$feature->feature}}">
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
    <a href="{{route('features.index')}}"><button>К списку</button></a>
@endsection