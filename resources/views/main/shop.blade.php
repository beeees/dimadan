@extends('layouts.shop')

@section('assets')
    @include('main.assets')
@endsection

@section('body')
    @include('main.nav')
    @include('main.presentation')
    @include('main.metro')
    @include('main.bags_sizes')
    @include('main.video')
    <div class="catalog-wrap" id="catalog">
        @include('main.bags_catalog')
        @include('main.podarok')
        @include('main.chehol_catalog')
    </div>
    @include('main.work_info')
    @include('main.distribution')
    @include('main.techology')
    @include('main.collage')
    @include('main.distribution_cost')
    @include('main.contacts')
    @include('main.address')
    {{--Bitrix24--}}
    <script data-skip-moving="true">
        (function (w, d, u, b) {
            s = d.createElement('script');
            r = (Date.now() / 1000 | 0);
            s.async = 1;
            s.src = u + '?' + r;
            h = d.getElementsByTagName('script')[0];
            h.parentNode.insertBefore(s, h);
        })(window, document, 'https://cdn.bitrix24.ru/b5147991/crm/site_button/loader_2_ex33ya.js');
    </script>
@endsection