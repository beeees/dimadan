<div class="b-prod">
    <div class="container">

        <div class="prod-right">
            <div class="prod-right-title">
                Наши преимущества
            </div>
        </div>
        <div class="prod-left row delivery-imgs">
            @foreach(\App\Models\DeliveryPicture::take(6)->get() as $p)
                <div class="col-3 delivery"><img src="{{Storage::url($p->picture)}}"></div>
            @endforeach
        </div>
        <div class="prod-right">
            <div class="prod-right-text" style="font-size: 15px; margin-top: 15px;">
                <yellow style="color:yellow; font-size: 18px">
                    {{--<p>--}}
                        {{--Мы обеспечиваем оперативную доставку по Москве.--}}
                    {{--</p>--}}
                    {{--<p>--}}
                        {{--Мы сотрудничаем с крупнейшими поставщиками высококачественной продукции, что позволяет нам--}}
                        {{--обеспечить максимально низкие цены.--}}
                    {{--</p>--}}
                    <p>
                        Преимущества покупки товаров в нашем магазине:
                    </p>
                    <p>
                        Вы можете сделать заказ, не отходя от компьютера, причем заказ на нашем сайте сделать гораздо проще,
                        чем в любом другом магазине.
                    </p>
                    <p>
                        Наши курьеры доставят Вашу покупку в удобное для Вас время. Наш ассортимент удовлетворит самых
                        взыскательных покупателей.
                    </p>
                    <p>
                        Наши цены ниже не только цен любого "реального" магазина, но и цен любого виртуального!
                    </p>
                    <p>
                        Доставка осуществляется во все регионы России!<br>
                        Гарантия 1 год.<br>
                        {{--Бесплатная доставка--}}
                    </p>
                    <p>
                        Самовывоз в Москве: <br>
                        город Москва, Яблочкова 19, офис 205;
                    </p>
                </yellow>
                <!--< div class="shop-bg-box-body">
                    <div class="shop-bg-box-body-t1">
                        Не можете определиться с размером и цветом? Приезжайте в наш магазин, и Вам подскажут
                        оптимальный вариант. Время работы магазина самовывоза — 9:00-22:00
                    </div>
                    <div class="shop-bg-box-body-t2">
                        На месте Вы также сможете протестировать изделие: чемодан можно бросать, топтать — делайте все,
                        что пожелаете!
                    </div>
                </div> -->
            </div>
        </div>

        <div class="prod-left row certificate col-1" style="padding-top: -10px;">
            <div class="w700">
                <div style="min-width: 100%; text-align: center;">
                    <h2 class="uk-h1 prod-right-text" style="font-size: 30px;">
                        Адрес сборки чемоданов: г.Тула, ул.Толстого, 91
                        <br>
                        Весь товар сертифицирован
                    </h2>
                </div>
                <div class="col-2"><img src="img_handbage/certificate1.jpg" style="max-width: 90%;"></div>
                <div class="col-2"><img src="img_handbage/certificate2.jpg" style="max-width: 90%;"></div>
            </div>
        </div>
    </div>
</div>