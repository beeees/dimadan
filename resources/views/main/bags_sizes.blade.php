
<div class="page wrapper">
    <div class="page clear">
        <div class="page block module advantages" id="advantages">
            <div class="page main-raw raw content wrap">
                <div class="page content-list clear row m-0 px-3 px-md-0">
                    @foreach($bags as $bag)
                    <div class="page item preview col-md row mb-5 mb-md-0">
                        <div class="page media col-md-12 col-xl-6 offset-6 offset-md-0 p-xl-0 align-self-start align-self-md-end"
                             data-rel="rotate" data-folder="/application/media/images/preview/" data-size="{{$bag->size}}"
                             data-max="3" data-items="1,2,3,4" data-format="png">
                            <div class="page image">
                                <img data-image="" class="page img-fluid" src="{{Storage::url($bag->picture)}}" alt="">
                                <div class="page main-row-box-price-page">
                                    <div><span>{{$bag->cost}} Р</span></div>
                                </div>
                                <div class="page main-row-box-price-page old-price-page">
                                    <div><span>{{$bag->old_cost}} Р</span></div>
                                </div>
                            </div>
                            <!--<div class="page label"><img src="/application/media/img/label.png"></div>-->
                        </div>
                        <div class="page content offset-lg-4 offset-xl-0 col-9 col-sm-12 col-lg-8 col-xl-6 p-3">
                            <div class="page title">
                                <div style="float:left;"><span class="page size_title-page">{{$bag->name}}</span> </div>
                                <div class="page my_circle-page">
                                    <span>{{$bag->size}}</span>
                                </div>
                            </div>
                            <div class="page text">
                                <ul class="page orange">
                                    @foreach(\App\Models\MainBagFeature::where(['main_bag_id' => [0, $bag->id]])->get() as $feature)
                                        <li><span>{{$feature->feature}}</span></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="page controls">
                                <a href="#catalog" class="page btn btn-danger btn-block"><span>Перейти в каталог</span></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>