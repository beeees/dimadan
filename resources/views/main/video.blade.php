<div class="b-video">
    <div class="video-title">
        <span> Почему выбирают ЧЕМОДАН.CLUB </span>
    </div>
    <div class="container">
        <div class="content-list clear row col-md-4 col-lg-12 offset-md-8 offset-lg-0">
            <div class="item c-lg mb-1 mb-lg-0 row mx-0 p-0">
                <div class="media c-2 c-lg-12 p-0">
                    <img src="img_handbage/surety.svg">
                </div>
                <div class="title c-10 c-lg-12 text-lg-center text-uppercase"><span>Гарантия<br>1 год</span></div>
            </div>
            <div class="item c-lg mb-2 mb-lg-0 row mx-0 p-0">
                <div class="media c-2 c-lg-12 p-0">
                    <img src="img_handbage/moscow.svg">
                </div>
                <div class="title c-10 c-lg-12 text-lg-center text-uppercase"><span>Российский<br>производитель</span></div>
            </div>
            <div class="item c-lg mb-3 mb-lg-0 row mx-0 p-0">
                <div class="media c-2 c-lg-12 p-0 align-content-center">
                    <img src="img_handbage/diamond.svg">
                </div>
                <div class="title c-10 c-lg-12 text-lg-center text-uppercase"><span>Роскошный<br>дизайн</span></div>
            </div>
            <div class="item c-lg mb-4 mb-lg-0 row mx-0 p-0">
                <div class="media c-2 c-lg-12 p-0">
                    <img src="img_handbage/boxing_glove.svg">
                </div>
                <div class="title c-10 c-lg-12 text-lg-center text-uppercase"><span>Неубиваемый<br>чемодан</span></div>
            </div>
            <div class="item c-lg mb-5 mb-lg-0 row mx-0 p-0">
                <div class="media c-2 c-lg-12 p-0">
                    <img src="img_handbage/purse.svg">
                </div>
                <div class="title c-10 c-lg-12 text-lg-center text-uppercase"><span>Лучшая<br>цена</span></div>
            </div>
        </div>
        <div class="video-row row">
            <!-- <div class="video-row-left">
                <div class="youtube-r">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/9RvIhFj4rpA" frameborder="0"
                            allowfullscreen></iframe>
                </div>
            </div> -->
            <div class="video-row-right">
                <div class="youtube-r">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/9RvIhFj4rpA" frameborder="0"
                            allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>