<div class="tm-copyright">
    <div class="uk-container uk-container-center">
        <div class="uk-clearfix">
            <div class="uk-float-right"></div>
            <div class="uk-float-left">
                ООО "Саквояж"
                <div class="uk-text-small uk-text-muted uk-margin-small-top">
                    город Москва, Яблочкова 19, офис 205<br>
                </div>
            </div>
        </div>
    </div>
</div>