<div class="b-rev">
    <div class="container">
        <div class="rev-foto row">
            <div id="collage" class="clear">
                @foreach(\App\Models\CatalogPicture::where(['category' => 'catalog1'])->get() as $p)
                    <img width="{{($i = \Intervention\Image\Facades\Image::make(Storage::disk('public')->path($p->picture)))->width()}}px" height="{{$i->height()}}px"  src="{{Storage::url($p->picture)}}">
                @endforeach
                {{--<img width="426" height="535"  src="img_handbage/rev/2.jpeg">--}}
                {{--<img width="426" height="535"  src="img_handbage/rev/3.jpeg">--}}
                {{--<img width="426" height="535"  src="img_handbage/rev/4.jpg">--}}
                {{--<img width="426" height="535"  src="img_handbage/rev/5.jpeg">--}}
                {{--<img width="426" height="535"  src="img_handbage/rev/6.jpeg">--}}
                {{--<img width="780" height="780"  src="img_handbage/rev/15.jpg">--}}
                {{--<img width="1280" height="847"  src="img_handbage/rev/16.jpg">--}}
                {{--<img width="744" height="733"  src="img_handbage/rev/17.jpg">--}}
                {{--<img width="741" height="939"  src="img_handbage/rev/18.jpg">--}}
                {{--<img width="639" height="800"  src="img_handbage/rev/19.jpg">--}}
            </div>
            <div id="collage2" class="clear">
                @foreach(\App\Models\CatalogPicture::where(['category' => 'catalog2'])->get() as $p)
                    <img width="{{($i = \Intervention\Image\Facades\Image::make(Storage::disk('public')->path($p->picture)))->width()}}px" height="{{$i->height()}}px"  src="{{Storage::url($p->picture)}}">
                @endforeach
                {{--<img width="426" height="535"  src="img_handbage/rev/7.jpg">--}}
                {{--<img width="426" height="535"  src="img_handbage/rev/8.jpeg">--}}
                {{--<img width="710" height="887"  src="img_handbage/rev/9.jpg">--}}
                {{--<img width="883" height="960"  src="img_handbage/rev/10.jpg">--}}
                {{--<img width="1000" height="668"  src="img_handbage/rev/11.jpg">--}}
                {{--<img width="959" height="960"  src="img_handbage/rev/12.jpg">--}}
                {{--<img width="800" height="793"  src="img_handbage/rev/13.jpg">--}}
                {{--<img width="750" height="780"  src="img_handbage/rev/14.jpg">--}}
            </div>
        </div>
    </div>
</div>