<div class="b-top">
    <div class="container">
        <a href="#" class="menu-mobile-btn"><span></span></a>
        <div class="top-logo-wrap">
            <div class="top-logo">
                <img src="img_handbage/logo.png">
            </div>
            <div class="top-logo_text">
                Магазин качественных <br>
                чемоданов по лучшей цене! 😉
            </div>
        </div>
        <div class="top-menu">
            <ul>
                <li><a href="#catalog"><span>КАТАЛОГ ПРОДУКЦИИ</span></a></li>
                <li><a href="#delivery"><span>ДОСТАВКА</span></a></li>
                <li><a href="#contacts"><span>КОНТАКТЫ</span></a></li>
            </ul>
        </div>
        <div class="top-phone">
            <div class="title">
                <a href="https://wa.me/79690421000" target="_blank"><i class="fa fa-fw fa-whatsapp fa-2x"
                                                                      aria-hidden="true"></i></a>
                <a href="viber://chat?number=79690421000"><i class="fa fa-fw fa-viber fa-2x"
                                                                  aria-hidden="true"></i></a>
            </div>
            <a class="phone_icon" href="tel:+79690421000">+7 (969) 042-10-00</a>

        </div>
    </div>
</div>