<div class="block5">
    <div class="main-bottom">
        <span>Каталог чемоданов</span>
    </div>
    <div class="block-2 catalog-section" id="budget">
        <div class="container">
            <div class="row">
                <div class="calalog-center">
                    <div class="item">
                        <div class="img calalog-center-border" style="text-align: center">
                            <img id="bag_main" src="{{Storage::url($firstBag->main_picture)}}" class="bag-center">
                            <img src="img_handbage/ring.svg" class="loader" alt="">
                        </div>
                        <div class="info calalog-info calalog-center-border">
                            <div class="calalog-info-left">
                                <div class="li">
                                    <a href="#" class="popover pop1" data-placement="top-right">Вмонтированный
                                        кодовый замок
                                    </a>
                                </div>
                                <div class="li">
                                    <a href="#" class="popover pop3" data-placement="top-right">Внутренний
                                        разделитель
                                    </a>
                                </div>
                                <div class="li">
                                    <a href="#" class="popover pop2" data-placement="top-left">Перекрестные
                                        ремни
                                    </a>
                                </div>
                                <div class="li">
                                    <a href="#" class="popover pop4" data-placement="top-left">4 колесика
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="catalog-left">
                    <div class="catalog-left-inner">
                        <div class="item item-1">
                            <div class="img">
                                <img id="bag_weight" src="{{Storage::url($firstBag->w_picture)}}" alt="">
                            </div>
                            <div class="info">
                                <p class="info-p1">Вес</p>
                                <p id='bagg_weight' class="info-p2 weight"></p>
                            </div>
                        </div>
                        <div class="item item-2">
                            <div class="img">
                                <img id="bag_v" src="{{Storage::url($firstBag->v_picture)}}" alt="">
                            </div>
                            <div class="info">
                                <p class="info-p1">Объем</p>
                                <p id='bag_vacuum' class="info-p2 volume"></p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img">
                                <img src="img_handbage/sizes.jpg" alt="">
                            </div>
                            <div class="info">
                                <p class="info-p1">Габариты</p>
                                <p id='bag_size' class="info-p2 dimensions"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="catalog-right">
                    <div class=" size">
                        <h5>Выберите размер</h5>
                        <div class="size-item">
                            <div id="bag-size" class="size-place">
                                <a id="size_s" onclick="set_size('s')"
                                   class="size-place__item"><span>S</span></a>
                                <a id="size_sp" onclick="set_size('sp')" class="size-place__item"><span>S+</span></a>
                                <a id="size_m" onclick="set_size('m')" class="size-place__item"><span>M</span></a>
                                <a id="size_mp" onclick="set_size('mp')" class="size-place__item"><span>M+</span></a>
                                <a id="size_l" onclick="set_size('l')" class="size-place__item"><span>L</span></a>
                                <a id="size_lp" onclick="set_size('lp')" class="size-place__item"><span>L+</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="color">
                        <h5>Выберите Цвет</h5>
                        <ul id="bag-color" class="list-unstyled list-inline">
                            @foreach($bagColors as $color)
                                <li data-color="{{$color->name}}"
                                    data-main="{{Storage::url($color->main_picture)}}"
                                    data-w="{{Storage::url($color->w_picture)}}"
                                    data-v="{{Storage::url($color->v_picture)}}"
                                    data-picture="{{$color->is_picture}}"
                                @if (preg_match('/^#[a-f0-9A-F]{6}$/i', $color->class))
                                    style="background-color: {{$color->class}};"
                                @else
                                    style="
                                        background: url({{Storage::url($color->icon)}}) no-repeat 50% 50%;
                                        background-size: 100%;"
                                @endif
                                    class="color-{{$color->class}}" ></li>
                            @endforeach
                        </ul>

                    </div>
                    <div class="price">
                        <h5>Стоимость</h5>
                        <p class="price-item"><strong id="bag_price">0</strong> <span>руб</span></p>
                        <div class="buttons">
                            <form method="post" class="lead">
                                <input type="hidden" name="DATA[TITLE]" value="Заявка с сайта">
                                <input type="hidden" name="DATA[OBJECT]" value="Чемодан">
                                <input type="hidden" name="DATA[SIZE]" id="order_bag_size" required/>
                                <input type="hidden" name="DATA[COLOR]" id="order_bag_color" required/>
                                <button style="background: transparent">
                                    <a class="js-show-popup-order">Заказать</a>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>