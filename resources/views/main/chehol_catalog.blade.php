<div class="block5 catalog_ch">
    <div class="block5-ch-title">
        <span>Чехлы для чемоданов</span>
    </div>
    <div class="block-2 block-5" id="covers">
        <div class="container">
            <div class="row">
                <div class="catalog_ch-left">
                    <ul id="ch-color" class="item">
                        @foreach($chColors as $color)
                            <li data-color="{{$color->name}}" class="img">
                                <img src="{{Storage::url($color->main_picture)}}">
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="catalog_ch-center">
                    <div class="item center-block">
                        <div class="img">
                            <img src="img_handbage/ring.svg" class="loader">
                            <img id="cover_main" src="{{Storage::url($firstCh->main_picture)}}"
                                 class="bag-center img-responsive center-block">
                        </div>
                    </div>
                </div>
                <div class="catalog_ch-right">
                    <div class="size">
                        <h5>Выберите размер</h5>
                        <div id="ch-size" class="size-place">
                            <a id="ch_size_s" data-size="s" class="size-place__item"><span>S</span>37 л</a>
                            <a id="ch_size_m" data-size="m" class="size-place__item"><span>M</span>56 л</a>
                            <a id="ch_size_l" data-size="l" class="size-place__item"><span>L</span>89 л</a>
                        </div>
                    </div>
                    <div class="price">
                        <h5>Стоимость</h5>
                        <p class="price-item"><strong id="ch_price">0</strong> <span>руб</span></p>

                        <form method="post" class="lead">
                            <input type="hidden" name="DATA[TITLE]" value="Заявка с сайта">
                            <input type="hidden" name="DATA[OBJECT]" value="Чехол">
                            <input type="hidden" id="order_ch_size" name="DATA[SIZE]" required/>
                            <input type="hidden" id="order_ch_color" name="DATA[COLOR]" required/>
                            <button class="buttons" style="background: transparent">
                                <a class="js-show-popup-ocover">Заказать</a>
                            </button>
                        </form>
                    </div>
                    <p class="span__red"><span></span></p>
                </div>
            </div>
        </div>
    </div>
</div>