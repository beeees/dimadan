<div class="b-podarok">
    <div class="container">
        <div class="podarok-right">
            <div class="podarok-right-title">
                Акция! Комплект из трёх чемоданов (S+M+L)
                <span>всего за 6600₽. </span>
            </div>
        </div>
    </div>
    <div class="podarok-line"></div>
    <div class="container podarok-container-bottom">
        <div class="podarok-img">
            <img src="img_handbage/podarki.png">
        </div>
        <div class="podarok-right">
            <div class="podarok-right-text">
                По вопросам участия в акции обращаейтесь по телефону.
            </div>
        </div>
    </div>
</div>