<div class="b-main uk-block-primary ">
    <div class="tm-block-stages  uk-contrast container">

        <h2 class="uk-h1 uk-margin-left">Как мы работаем</h2>
        <div class="main-row row">
            <div class="col-3">
                <img src="img_handbage/step1.png">
                <div class="uk-panel">
                    <i style="margin:15px 0 0 41%;">1</i>
                </div>
                <div style="display: block; margin-top: 80px;">
                    <p class="uk-h4">Заполните заявку на нашем сайте</p>
                </div>
            </div>
            <div class="col-3">
                <img src="img_handbage/step2.png">
                <div class="uk-panel">
                    <i style="margin:15px 0 0 44.5%;">2</i>
                </div>
                <div style="display: block; margin-top: 80px;">
                    <p class="uk-h4">Дождитесь одобрения заявки</p>
                </div>
            </div>
            <div class="col-3">
                <img src="img_handbage/step3.png">
                <div class="uk-panel" style="display: block">
                    <i style="margin:15px 0 0 43%;">3</i>
                </div>
                <div style="display: block; margin-top: 80px;">
                    <p class="uk-h4">Оплатите стоимость товара курьеру</p>
                </div>
            </div>
        </div>
    </div>
</div>