<div class="b-main">
    <div class="container">
        <div class="main-top-wrap">
            <div class="main-top">
                <a href="#catalog" class="main-top-btn js-to">
                    <div class="main-metro">
                        <span class="main-metro-left">
                            <span>Самовывоз:</span>
                            <span class="main-metro-right">
                              Тимирязевская
                            </span>
                        </span>
                    </div>
                </a>
                <span class="main-top-yellow">Режим работы: 24/7. Акция: <red style="color: red">Подарок - подушка для путешествий!</red></span>
            </div>
        </div>
    </div>
</div>