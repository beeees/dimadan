<div class="b-teh">
    <div class="block module static bg-white advantage-blocks radial-gradient container">
        <div class="head">
            <div class="title black text-uppercase teh-title"><span>О чемодане</span></div>
        </div>
        <div class="content wrap row px-3 px-md-0">
            <div class="col-md-6 col-lg-5 col-xl-4">
                <div class="block mb-3 mb-md-4 bg-white">
                    <div class="media d-block">
                        <div class="image"><img src="/img_handbage/tech-1.jpg"></div>
                        <div class="index right text-white"><span>1</span></div>
                    </div>
                    <div class="text p-3">
                        <div class="title text-lg-right pb-2"><span>Кодовый замок</span></div>
                        <div class="description text-justify small"><span>Встроенный кодовый замок обеспечит сохранность ваших вещей во время путешествия. получить доступ к багажу может только тот, кто знает код – уникальную комбинацию цифр.</span></div>
                    </div>
                </div>
                <div class="block mb-3 mb-md-4 bg-white">
                    <div class="media d-block">
                        <div class="image"><img src="/img_handbage/tech-2.jpg"></div>
                        <div class="index right text-white"><span>2</span></div>
                    </div>
                    <div class="text p-3">
                        <div class="title text-lg-right pb-2"><span>Молния</span></div>
                        <div class="description text-justify small"><span>Боковая молния расположена таким образом, чтобы вы могли легко и быстро открыть чемодан. Резиновый ободок препятствует попаданию влаги и защищает чемодан от взлома.</span></div>
                    </div>
                </div>
                <div class="block mb-3 mb-md-4 bg-white">
                    <div class="media d-block">
                        <div class="image"><img src="/img_handbage/tech-3.jpg"></div>
                        <div class="index right text-white"><span>3</span></div>
                    </div>
                    <div class="text p-3">
                        <div class="title text-lg-right pb-2"><span>Переносная ручка</span></div>
                        <div class="description text-justify small"><span>Удобная ручка из прочного пластика предназначена для ручной переноски багажа. если нет возможности катить чемодан на колесиках – воспользуйтесь переносной ручкой.</span></div>
                    </div>
                </div>
            </div>
            <div class="colf-md-0 col-lg-2 col-xl-4 p-md-0 p-lg-auto d-none d-md-block">
                <div class="media d-none d-sm-block"><img src="/img_handbage/tech-main.png"></div>
            </div>
            <div class="col-md-6 col-lg-5 col-xl-4">
                <div class="block mb-3 mb-md-4 bg-white">
                    <div class="media d-block">
                        <div class="image"><img src="/img_handbage/tech-4.jpg"></div>
                        <div class="index left text-white"><span>4</span></div>
                    </div>
                    <div class="text p-3">
                        <div class="title text-lg-left pb-2"><span>Выдвижная ручка</span></div>
                        <div class="description text-justify small"><span>Чемоданы на колесиках оснащены телескопической выдвижной ручкой. вы можете выдвинуть ручку и зафиксировать в одном из нескольких положений, подобрав комфортную длину.</span></div>
                    </div>
                </div>
                <div class="block mb-3 mb-md-4 bg-white">
                    <div class="media d-block">
                        <div class="image"><img src="/img_handbage/tech-5.jpg"></div>
                        <div class="index left text-white"><span>5</span></div>
                    </div>
                    <div class="text p-3">
                        <div class="title text-lg-left pb-2"><span>Корпус</span></div>
                        <div class="description text-justify small"><span>Корпус, изготовленный из abs-пластика – идеальное решение для российских погодных условий. Пластиковый чемодан устойчив к морозам и способен выдержать нагрузку до 120 кг.</span></div>
                    </div>
                </div>
                <div class="block mb-3 mb-md-4 bg-white">
                    <div class="media d-block">
                        <div class="image"><img src="/img_handbage/tech-6.jpg"></div>
                        <div class="index left text-white"><span>6</span></div>
                    </div>
                    <div class="text p-3">
                        <div class="title text-lg-left pb-2"><span>Колесики</span></div>
                        <div class="description text-justify small"><span>Резиновые колесики с подшипниками обеспечивают комфортное передвижение чемодана по любой поверхности. Колесики изготовлены из долговечной резины, устойчивой к истиранию.</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>