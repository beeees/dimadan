<div class="b-contact" id="contacts">
    <div class="container">
        <div class="row">
            <div class="contact-right">
                <div class="contact-right-row">
                    <div class="contact-right-row-item row">
                        <div class="contact-right-row-left">Адрес:</div>
                        <div class="contact-right-row-right">
                            Россия, город Москва,<br/>
                            Яблочкова 19, офис 205, вход с торца здания
                        </div>
                    </div>
                    <div class="contact-right-row-item row">
                        <div class="contact-right-row-left">Телефон:</div>
                        <div class="contact-right-row-right">
                            <a href="tel:+79690421000">+7 (969) 042-10-00</a>
                        </div>
                    </div>
                    <div class="contact-right-row-item row">
                        <div class="contact-right-row-left">График работы:</div>
                        <div class="contact-right-row-right">
                            с 9-00 до 22-00 с пн. по вс.
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-map">
                <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A074c4e3eef0533b8e0782ffa7b88925f711d480cf7f3171da6a599480b512dd4&amp;width=600&amp;height=430&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </div>
    </div>
</div>