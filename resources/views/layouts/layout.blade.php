<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <style>
        body {
            vertical-align: top;
            text-align: center;

        }
        body tr td, body tr th {
            padding: 10px;
        }
    </style>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

</head>
<body>
<div class="flex-center position-ref full-height">
    <ul style="position: absolute; width: 100px; height: 100%; right: 0; border-left: 2px solid">
        <h5>Меню</h5>
        <br>
        <li><a href="{{route('root')}}">Сайт</a> </li>
        <li><a href="{{route('main-bags.index')}}">Реклама чемоданов</a> </li>
        <li><a href="{{route('features.index')}}">Преимущества чемоданов</a> </li>
        <li><a href="{{route('bag-colors.index')}}">Каталог чемоданов</a> </li>
        <li><a href="{{route('chechol-colors.index')}}">Каталог чехлов</a> </li>
        <li><a href="{{route('pictures.index')}}">Изображения</a> </li>
    </ul>
    <div class="content">
        @yield('content')
    </div>
</div>
</body>
</html>
