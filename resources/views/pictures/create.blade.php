@extends('layouts.layout')

@section('content')
    <h1>Добавление картинки</h1>
    <hr>
    <form action="{{url('pictures')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Категория</label>
            <select name="category" id="name">
                <option value="delivery">раздел доставка</option>
                <option value="catalog1">раздел каталог</option>
                <option value="catalog2">раздел каталог2</option>
            </select>
        </div>
        <div class="form-group">
            <label for="picture">Картинка</label>
            <input type="file" width="150px" height="100px" class="form-control" id="picture"
                   name="picture">
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
    <a href="{{route('pictures.index')}}"><button>К списку</button></a>
@endsection