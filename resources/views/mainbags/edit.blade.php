@extends('layouts.layout')

@section('content')
    <h1>Изменение размера</h1>
    <hr>
    <form action="{{url('main-bags', [$bag->id])}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div>
            <img src="{{Storage::url($bag->picture)}}">
        </div>
        <div class="form-group">
            <label for="size">Размер</label>
            <input type="text" class="form-control" id="size" name="size" value="{{$bag->size}}">
        </div>
        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$bag->name}}">
        </div>
        <div class="form-group">
            <label for="picture">Картинка</label>
            <input type="file" width="150px" height="100px" class="form-control" id="picture"
                   name="picture">
        </div>
        <div class="form-group">
            <label for="cost">Цена</label>
            <input type="text" class="form-control" id="cost" name="cost" value="{{$bag->cost}}">
        </div>
        <div class="form-group">
            <label for="old_cost">Cтарая цена</label>
            <input type="text" class="form-control" id="old_cost" name="old_cost" value="{{$bag->old_cost}}">
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
    <a href="{{route('main-bags.index')}}"><button>К списку</button></a>
@endsection