@extends('layouts.layout')

@section('content')
    <h1>Размеры чемоданов</h1>
    <a href="{{url('main-bags/create')}}">Добавить размер</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Размер</th>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
            <th scope="col">Картинка</th>
            <th scope="col">Изменить</th>
            <th scope="col">Удалить</th>
        </tr>
        </thead>
        <tbody>
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        @foreach($bags as $bag)
            <tr>
                <td scope="row" style="text-align: left;"><a href="{{url('main-bags', [$bag->id])}}">{{$bag->size}}</a></td>
                <td scope="row">{{$bag->name}}</td>
                <td scope="row">{{$bag->cost}}</td>
                <td scope="row"><img src="{{Storage::disk('local')->url($bag->picture)}}" style="max-height: 100px; border: 1px solid;"></td>
                <td><a href="{{url('main-bags', [$bag->id, 'edit'])}}">Изменить</a></td>
                <td>
                    <form action="{{url('main-bags', [$bag->id])}}" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" class="btn btn-danger" value="Удалить"/>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
