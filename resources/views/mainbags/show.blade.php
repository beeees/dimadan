@extends('layouts.layout')

@section('content')
    <h1>Просмотр размера</h1>
    <hr>
    <form action="{{url('main-bags')}}" method="post">
        {{ csrf_field() }}
        <div>
            <img src="{{Storage::url($bag->picture)}}">
        </div>
        <div class="form-group">
            <label for="size">Размер</label>
            <input type="text" class="form-control" id="size" name="size" value="{{$bag->size}}">
        </div>
        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" width="150px" height="100px" class="form-control" id="name"
                   name="name" value="{{$bag->name}}">
        </div>
        <div class="form-group">
            <label for="cost">Цена</label>
            <input type="text" class="form-control" id="cost" name="cost" value="{{$bag->cost}}">
        </div>
        <div class="form-group">
            <label for="old_cost">Cтарая цена</label>
            <input type="text" class="form-control" id="old_cost" name="old_cost" value="{{$bag->old_cost}}">
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </form>
    <a href="{{route('main-bags.index')}}"><button>К списку</button></a>
@endsection