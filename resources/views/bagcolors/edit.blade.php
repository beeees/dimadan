@extends('layouts.layout')

@section('content')
    <h1>Редактирование цвета чемодана</h1>
    <hr>
    <form action="{{url('bag-colors', [$color->id])}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{method_field('PUT')}}
        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$color->name}}">
        </div>
        <div class="form-group">
            <label for="class">Английское название</label>
            <input type="text" class="form-control" id="class" name="class" value="{{$color->class}}">
        </div>
        <div class="form-group">
            <label for="is_picture">Чемодан с картинкой</label>
            <input type="checkbox" class="form-control" id="is_picture"
                   name="is_picture" {{$color->is_picture ? 'checked' : ''}}>
        </div>
        <div class="form-group">
            <label for="main_picture">Основная картинка</label>
            <div>
                <img style="max-height: 150px" src="{{Storage::url($color->main_picture)}}">
            </div>
            <input type="file" width="150px" height="100px" class="form-control" id="main_picture"
                   name="main_picture">
        </div>
        <div class="form-group">
            <label for="w_picture">Вес-картинка</label>
            <div>
                <img style="max-height: 150px" src="{{Storage::url($color->w_picture)}}">
            </div>
            <input type="file" width="150px" height="100px" class="form-control" id="w_picture"
                   name="w_picture">
        </div>
        <div class="form-group">
            <label for="v_picture">Объем-картинка</label>
            <div>
                <img style="max-height: 150px" src="{{Storage::url($color->v_picture)}}">
            </div>
            <input type="file" width="150px" height="100px" class="form-control" id="v_picture"
                   name="v_picture">
        </div>
        <div class="form-group">
            <label for="icon">Иконка </label>
            <div>
                <img style="max-height: 150px" src="{{Storage::url($color->icon)}}">
            </div>
            <input type="file" width="150px" height="100px" class="form-control" id="icon"
                   name="icon">
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
    <a href="{{route('bag-colors.index')}}"><button>К списку</button></a>
@endsection