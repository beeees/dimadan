@extends('layouts.layout')

@section('content')
    <h1>Цвета чемоданов</h1>
    <a href="{{url('bag-colors/create')}}">Добавить цвет чемодана</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Название</th>
            <th scope="col">Англ. название</th>
            <th scope="col">Иконка</th>
            <th scope="col">Изменить</th>
            <th scope="col">Удалить</th>
        </tr>
        </thead>
        <tbody>
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        @foreach($colors as $color)
            <tr>
                <td scope="row" style="text-align: left;">{{$color->name}}</td>
                <td scope="row">{{$color->class}}</td>
                <td scope="row">
                    @if (preg_match('/^#[a-f0-9A-F]{6}$/i', $color->class))
                        <div style="width: 40px; border-radius:50%; height: 40px; background-color: {{$color->class}}"></div>
                    @else
                        <div>
                            <img style="width: 40px; border-radius:50%; height: 40px;" src="{{Storage::url($color->icon)}}">
                        </div>
                    @endif
                </td>
                <td><a href="{{url('bag-colors', [$color->id, 'edit'])}}">Изменить</a></td>
                <td>
                    <form action="{{url('bag-colors', [$color->id])}}" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" class="btn btn-danger" value="Удалить"/>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
