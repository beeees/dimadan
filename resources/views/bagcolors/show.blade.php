@extends('layouts.layout')

@section('content')
    <h1>Просмотр чемодана</h1>
    <hr>
    <div class="form-group">
        <label for="size">Название: {{$color->name}}</label>
    </div>
    <div class="form-group">
        <label for="size">Английское название: {{$color->class}}</label>
    </div>
    <div class="form-group">
        <label for="cost">Основная картинка:</label>
        <div>
            <img style="max-height:150px" src="{{Storage::url($color->main_picture)}}">
        </div>
    </div>
    <div class="form-group">
        <label for="cost">Вес-картинка:</label>
        <div>
            <img style="max-height:150px" src="{{Storage::url($color->w_picture)}}">
        </div>
    </div>
    <div class="form-group">
        <label for="cost">Объем-картинка:</label>
        <div>
            <img style="max-height:150px" src="{{Storage::url($color->v_picture)}}">
        </div>
    </div>
    <div class="form-group">
        <label for="cost">Иконка:</label>
        <div>
            <img style="max-height:150px" src="{{Storage::url($color->icon)}}">
        </div>
    </div>
    <a href="{{route('bag-colors.index')}}"><button>К списку</button></a>
@endsection