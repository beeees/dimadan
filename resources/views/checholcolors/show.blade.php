@extends('layouts.layout')

@section('content')
    <h1>Просмотр цвета чехла</h1>
    <hr>
    <div class="form-group">
        <label for="size">Название: {{$color->name}}</label>
    </div>
    <div class="form-group">
        <label for="cost">Основная картинка:</label>
        <div>
            <img style="max-height:150px" src="{{Storage::url($color->main_picture)}}">
        </div>
    </div>
    <a href="{{route('chechol-colors.index')}}"><button>К списку</button></a>
@endsection