@extends('layouts.layout')

@section('content')
    <h1>Цвета чехлов</h1>
    <a href="{{url('chechol-colors/create')}}">Добавить цвет чехла</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Название</th>
            <th scope="col">Иконка</th>
            <th scope="col">Изменить</th>
            <th scope="col">Удалить</th>
        </tr>
        </thead>
        <tbody>
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        @foreach($colors as $color)
            <tr>
                <td scope="row" style="text-align: left;">{{$color->name}}</td>
                <td scope="row">
                        <div>
                            <img style="width: 40px; border-radius:50%; height: 40px;" src="{{Storage::url($color->main_picture)}}">
                        </div>
                </td>
                <td><a href="{{url('chechol-colors', [$color->id, 'edit'])}}">Изменить</a></td>
                <td>
                    <form action="{{url('chechol-colors', [$color->id])}}" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" class="btn btn-danger" value="Удалить"/>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
