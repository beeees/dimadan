@extends('layouts.layout')

@section('content')
    <h1>Добавление цвета чемодана</h1>
    <hr>
    <form action="{{url('chechol-colors')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" id="name" name="name" value="{{request('name', '')}}">
        </div>
        <div class="form-group">
            <label for="main_picture">Основная картинка</label>
            <input type="file" width="150px" height="100px" class="form-control" id="main_picture"
                   name="main_picture">
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
    <a href="{{route('chechol-colors.index')}}"><button>К списку</button></a>
@endsection