<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@main')->name('root');
Route::get('/content-change-manager', 'Controller@manager')->name('manager');
Route::resource('main-bags', 'MainBagController');
Route::resource('features', 'MainBagFeatureController');
Route::resource('bag-colors', 'BagColorsController');
Route::resource('chechol-colors', 'ChecholColorsController');
Route::resource('pictures', 'PicturesController');
