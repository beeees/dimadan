<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MainBagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_cost' => 'required|integer',
            'picture' => 'required|mimes:jpg,bmp,png,jpeg',
            'cost' => 'required|integer',
            'size' => 'required|string|max:2|min:1',
        ];
    }
}
