<?php

namespace App\Http\Controllers;

use App\Http\Requests\MainBagRequestUpdate;
use App\Models\MainBag;
use App\Models\MainBagFeature;
use Illuminate\Http\Request;

class MainBagFeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('features.index', [
            'features' => MainBagFeature::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('features.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @internal param FormBuilder $formBuilder
     * @internal param Request $request
     */
    public function store(Request $request)
    {
        $params = $request->all();
        $feature = (new MainBagFeature())->fill($params);
        $feature->save();
        return view('features.show', compact('feature'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feature = MainBagFeature::find($id);
        return view('features.show', compact('feature'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $feature = MainBagFeature::find($id);
        return view('features.edit', compact('feature'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $feature = MainBagFeature::find($id);
        $feature->fill($request->all());
        $feature->save();
        return view('features.show', compact('feature'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MainBagFeature::find($id)->delete();
        return $this->index();
    }
}
