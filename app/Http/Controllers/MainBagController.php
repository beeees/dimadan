<?php

namespace App\Http\Controllers;

use App\Http\Requests\MainBagRequest;
use App\Http\Requests\MainBagRequestUpdate;
use App\Models\MainBag;
use Kris\LaravelFormBuilder\FormBuilder;

class MainBagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        var_dump(\Storage::url(MainBag::first()->picture));exit();
        return view('mainbags.index', [
            'bags' => MainBag::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param FormBuilder $form
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mainbags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MainBagRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @internal param FormBuilder $formBuilder
     * @internal param Request $request
     */
    public function store(MainBagRequest $request)
    {
        $params = $request->all();
        $bag = (new MainBag())->fill($params);
        $path = $request->file('picture')->store('main-bags', 'public');
        $bag->picture = $path;
        $bag->save();
        return view('mainbags.show', compact('bag'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bag = MainBag::find($id);
        return view('mainbags.show', compact('bag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $bag = MainBag::find($id);
        return view('mainbags.edit', compact('bag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MainBagRequestUpdate $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MainBagRequestUpdate $request, $id)
    {
        $bag = MainBag::find($id);
        $bag->fill($request->all());
        if ($request->hasFile('picture')) {
            $path = $request->file('picture')->store('main-bags', 'public');
            $bag->picture = $path;
        }
        $bag->save();
        return view('mainbags.show', compact('bag'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MainBag::find($id)->delete();
        return $this->index();
    }
}
