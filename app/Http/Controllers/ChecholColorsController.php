<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChecholColorUpdateRequest;
use App\Http\Requests\ChecholColorCreateRequest;
use App\Models\ChecholColor;
use Illuminate\Http\Request;

/**
 * Class ChecholColorsController
 * @package App\Http\Controllers
 */
class ChecholColorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('checholcolors.index', [
            'colors' => ChecholColor::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('checholcolors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ChecholColorCreateRequest|Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @internal param FormBuilder $formBuilder
     * @internal param Request $request
     */
    public function store(ChecholColorCreateRequest $request)
    {
        $params = $request->all();
        $color = (new ChecholColor())->fill($params);
        $color->setPictures($request);
        $color->save();
        return view('checholcolors.show', compact('color'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $color = ChecholColor::find($id);
        return view('checholcolors.show', compact('color'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $color = ChecholColor::find($id);
        return view('checholcolors.edit', compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ChecholColorUpdateRequest $request, $id)
    {
        $color = ChecholColor::find($id);
        $color->fill($request->all());
        $color->setPictures($request);
        $color->save();
        return view('checholcolors.show', compact('color'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ChecholColor::find($id)->delete();
        return $this->index();
    }
}
