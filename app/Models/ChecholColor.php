<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class ChecholColor
 * @package App\Models
 * @property String $name
 * @property String $main_picture
 */
class ChecholColor extends Model
{
    protected $fillable = [
        'name',
    ];

    public function setPictures(Request $request) {
        foreach ([
            'main_picture',
        ] as $picture) {
            if ($request->hasFile($picture)) {
                $this->$picture = $request->file($picture)->store('chechol-colors', 'public');
            }
        }
    }
}
